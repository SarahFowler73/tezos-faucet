/* Set up express */

const express  = require('express');
const app = express();
const bodyParser = require('body-parser');

app.set("view engine", "pug");

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static('public'))

/* Set up reCAPTCHA2 */
const reCAPTCHA = require('recaptcha2');

var recaptcha2Keys = require('./recaptcha_keys.json');

var recaptcha = new reCAPTCHA(recaptcha2Keys);

/* Set up the db and they keys */
const redis = require("redis");
const keys = require('./secret_seeds.json');
const db = redis.createClient(process.env.REDIS_URI);

console.log("KEYS: ", keys.length);

db.on("error", function (err) {
  console.log("Redis error encountered", err);
});

db.on("end", function() {
  console.log("Redis connection closed");
});

db.incr('counter');

app.get('/', (req, res) =>
        {
            console.log(recaptcha.formElement());
            res.render('index',
                       {
                           recaptcha_form:
`<div class="g-recaptcha" data-sitekey=${recaptcha2Keys.siteKey} data-callback=captchaDone></div>`
                       });
        }
       );

app.post('/', (req,res) => {
    recaptcha.validateRequest(req)
        .then(function(){
            // captcha validated and secure
            let key, seed, amount;
            db.get('counter',
                   function(err, i) {
                     if (!err) {
                       console.log("Serving key for counter ", i );
                       json    = keys[i];
                       key     = json.pkh;
                       console.log("Sending: ", i, key);
                       db.incr(
                           'counter',
                           function( err, i)  {
                               res.setHeader("Content-Disposition", `attachment; filename="${key}.json"`);
                               res.set('Content-Type', 'application/json');
                               res.send(JSON.stringify(json, null, 2));
                           });
                     } else {
                       console.log("error connecting to db for counter ", i, err)
                     }
                   });
        })
        .catch(function(errorCodes){
            // invalid
            res.json({formSubmit:false,errors:recaptcha.translateErrors(errorCodes)});// translate error codes to human readable text
        });
});

app.listen(8081);
